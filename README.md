# Documentation

for collaboration and more detailed informations:

email: eqsoft4@gmail.com

## Overview

- The main purpose is coping with complex development and test requirements on local and remote hosts by streamlining multi composer stacks and pre-configured online IDEs based on openvscode-server.

- Ecosystem for configuration, deployment, management and launch of container stacks based on native technologies such as git, go, ssh and docker-compose

- Appstack supports static deployments on single-node environments and is not comparable to multi-node container orchestrator platforms like k8s or Docker Swarm

- Appstack can be used with custom applications based on custom images or Composer stacks from external application catalogs

- A public [application catalog](https://gitlab.com/eqsoft-appstack/apps) with few apps and services focusing on developing e-learning applications already exists and will be gradually extended

## Technical Overview

![technical overview of appstack](images/appstack.png "Technical Overview")

## Overview Appstack Apps

An appstack app is just a git repo with an appstack configuration file (`config.yaml`) a composer template (`docker-compose.yaml.tmpl`) and optional bootstrapping scripts (`*.sh`). Example:

```
├── cleanup.sh
├── config.yaml
├── docker-compose.yaml.tmpl
├── init.sh
└── setup.sh
```

## Overview Appstack Services

- Appstack Services are optimized container images for running in the appstack composer stacks, but can also be used standalone or in other orchestrators like kubernetes.

- The main target of the optimization is streamlining the underlying OS (Debian/Ubuntu) and setting fix UID and GID to www-data (33:33).

- Additional internal or external volumes can be mounted with predictable read and write permissions p.e. into the online vscode service, which is also running as www-data user.

- Automated building with [autobuilderx](https://gitlab.com/eqsoft-appstack/autobuilderx): daily check-ups of the underlying base image tag for updated upstream sha256 digest or check  upgradable debian packages which triggers CI pipline for (re-)building, testing, (re-)tagging and pushing new service image to dockerhub https://hub.docker.com/u/eqsoft4